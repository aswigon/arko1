#
# Artur J. Świgoń <aswigon@aswigon.pl>
#

MARS=java -jar mars.jar
MARSFLAGS=db nc

SPIM=spim
SPIMFLAGS=-delayed_branches -delayed_loads -noexception

mars:
	$(MARS) $(MARSFLAGS) bezier.asm

spim:
	$(SPIM) $(SPIMFLAGS) -file bezier.asm
