#
# Artur J. Świgoń <aswigon@aswigon.pl>
#

#
# Program wymaga skoków opóźnionych i działa dla opóźnionego ładowania
# Uruchamianie:
# $ java -jar mars.jar db nc bezier.asm
# $ spim -delayed_branches -delayed_loads -noexception -file bezier.asm
#
# SPIM może mieć problem z utworzeniem nowego pliku. Może być konieczne
# utworzenie go ręcznie (touch).
#

    .globl __start

    .data

interactive:
    # Wartość niezerowa oznacza tryb interakcji z użytkownikiem
    .byte 1

filename:
    # Nazwa pliku wynikowego (11 bajtów)
    .asciiz "bezier.bmp"
    # Plus miejsce na dłuższe nazwy (razem 64 bajty)
    .space 53

width:
    # Szerokość obrazka w pikselach
    .half 1920

height:
    # Wysokość obrazka w pikselach
    .half 1080

points:
    # Współrzędne punktów p0, p1, p2, gdzie pn = (xn, yn)
    .half 200, 100
    .half 2300, 300
    .half 1200, 900

background:
    # Kolor tła
    .word 0x00FFFFFF

foreground:
    # Kolor krzywej
    .word 0x00AA00CC

step:
    # Wielkość delta-t dla krzywej
    # (część ułamkowa liczby stałoprzecinkowej)
    .half 0x0010

    .text
    # Początek programu
__start:
    la $a0, interactive
    # Inicjalizacja ramki stosu
    move $fp, $sp

    lbu $t0, 0($a0)
    nop
    beqz $t0, fopen_try
    nop

    .data
prompt_filename:
    .asciiz "Filename: "
prompt_width:
    .asciiz "Image width in pixels: "
prompt_height:
    .asciiz "Image height in pixels: "
prompt_x0:
    .asciiz "x0: "
prompt_y0:
    .asciiz "y0: "
prompt_x1:
    .asciiz "x1: "
prompt_y1:
    .asciiz "y1: "
prompt_x2:
    .asciiz "x2: "
prompt_y2:
    .asciiz "y2: "

    .text
    # Pobranie nazwy pliku
    la $a0, prompt_filename
    li $v0, 4
    syscall
    la $a0, filename
    li $a1, 64
    li $v0, 8
    syscall

    la $t0, filename
    li $t2, 0x0A

strip_newline_loop:
    # Usunięcie znaku nowej linii
    lbu $t1, 0($t0)
    nop
    beq $t1, $t2, strip_newline_do
    nop
    bnez $t1, strip_newline_loop
    addiu $t0, $t0, 1
    j strip_newline_end
    nop

strip_newline_do:
    sb $zero, 0($t0)

strip_newline_end:
    # Pobranie szerokości
    la $a0, prompt_width
    li $v0, 4
    syscall
    li $v0, 5
    syscall
    la $a0, width
    sh $v0, 0($a0)

    # Pobranie wysokości
    la $a0, prompt_height
    li $v0, 4
    syscall
    li $v0, 5
    syscall
    la $a0, height
    sh $v0, 0($a0)

    # Pobieranie współrzędnych
    la $t0, points

    # Pobranie x0
    la $a0, prompt_x0
    li $v0, 4
    syscall
    li $v0, 5
    syscall
    sh $v0, 0($t0)

    # Pobranie y0
    la $a0, prompt_y0
    li $v0, 4
    syscall
    li $v0, 5
    syscall
    sh $v0, 2($t0)

    # Pobranie x1
    la $a0, prompt_x1
    li $v0, 4
    syscall
    li $v0, 5
    syscall
    sh $v0, 4($t0)

    # Pobranie y1
    la $a0, prompt_y1
    li $v0, 4
    syscall
    li $v0, 5
    syscall
    sh $v0, 6($t0)

    # Pobranie x2
    la $a0, prompt_x2
    li $v0, 4
    syscall
    li $v0, 5
    syscall
    sh $v0, 8($t0)

    # Pobranie y2
    la $a0, prompt_y2
    li $v0, 4
    syscall
    li $v0, 5
    syscall
    sh $v0, 10($t0)

fopen_try:
    # Próba otwarcia pliku do zapisu
    la $a0, filename
    li $a1, 1
    li $a2, 0
    li $v0, 13
    syscall

    bltz $v0, fopen_error
    # Odłożenie deskryptora pliku na stosie
    sw $v0, 0($fp)

calc_size1:
    # Wyliczenie rozmiaru obrazka w bajtach
    # Załadowanie szerokości i wysokości do $s0, $s1
    la $t0, width
    lhu $s0, 0($t0)
    la $t0, height
    lhu $s1, 0($t0)

    # Reszta z dzielenia szerokości przez 32
    andi $t0, $s0, 0x1F
    beqz $t0, calc_size2
    move $s3, $s0
    li $t1, 32
    # Wyznaczenie (32 - reszta)
    subu $t0, $t1, $t0
    addu $s3, $s0, $t0

calc_size2:
    # $s3 zawiera teraz szerokość uwzględniającą wyrównanie
    # Rozmiar obrazka w bajtach to (width / 8) * height
    sra $s3, $s3, 3
    # Zapamiętanie $s3 w rejestrze $s7
    move $s7, $s3
    multu $s3, $s1
    mfhi $t0
    bnez $t0, calc_size_error
    mflo $s4
    # $s4 zawiera teraz rozmiar obrazka w bajtach
    # Odłożenie rozmiaru bufora danych na stosie
    addiu $sp, $sp, -4
    sw $s4, -4($fp)

malloc_try:
    # Próba alokacji pamięci bufora danych
    li $v0, 9
    move $a0, $s4
    syscall

    beqz $v0, malloc_error
    # Odłożenie adresu bufora na stosie
    sw $v0, -8($fp)
    addiu $sp, $sp, -4

    # Odłożenie wcześniej zapamiętanej w $s7 szerokości z wyrównaniem
    sw $s7, -12($fp)
    addiu $sp, $sp, -4

init_header:
    # Stworzenie nagłówka pliku
    addiu $sp, $sp, -64
    # $s0 posłuży za rejestr adresowy
    addiu $s0, $sp, 2

    # 'B' 'M' na początku pliku
    li $t0, 0x4D42
    lw $t1, -4($fp)
    sh $t0, 0($s0)
    # Rozmiar pliku
    addiu $t0, $t1, 62
    sw $t0, 2($s0)
    # Dwa zerowe półsłowa
    sw $zero, 6($s0)
    # Położenie danych
    li $t0, 62
    sw $t0, 10($s0)
    # Wielkość drugiej części nagłówka
    li $t1, 40
    sw $t1, 14($s0)
    # Szerokość (i wysokość) bitmapy
    la $t1, width
    lhu $t3, 0($t1)
    la $t2, height
    lhu $t4, 0($t2)
    sw $t3, 18($s0)
    sw $t4, 22($s0)
    # Liczba warstw
    li $t0, 1
    sh $t0, 26($s0)
    # Liczba bitów na piksel
    sh $t0, 28($s0)
    # Metoda kompresji
    sw $zero, 30($s0)
    # Rozmiar danych (0 dla nieskompresowanych)
    sw $zero, 34($s0)
    # Rozdzielczość pozioma (i pionowa)
    sw $zero, 38($s0)
    sw $zero, 42($s0)
    # Liczba kolorów (0 oznacza 2^n)
    sw $zero, 46($s0)
    # Liczba ważnych kolorów (0 oznacza wszystkie ważne)
    sw $zero, 50($s0)
    # Tablica kolorów
    lw $t0, background
    lw $t1, foreground
    sw $t0, 54($s0)
    sw $t1, 58($s0)

write_header:
    # Zapisanie nagłówka do pliku
    lw $a0, 0($fp)
    move $a1, $s0
    li $a2, 62
    li $v0, 15
    syscall

    bltz $v0, fwrite_error
    addiu $sp, $sp, 64

draw_curve:
    # $s0 - bazowa maska (lewy piksel bajtu)
    # $s1 - szerokość obrazka
    # $s2 - wysokość obrazka
    # $s3 - szerokość wiersza w bajtach
    # $s4 - adres bufora
    # $s5 - delta-t
    # $s6 - aktualne t
    # $s7 - wartość jeden (graniczne t)
    lw $s4, -8($fp)
    la $t0, step
    lhu $s5, 0($t0)
    li $s6, 0
    li $s7, 0x00010000
    la $t0, width
    lhu $s1, 0($t0)
    la $t0, height
    lhu $s2, 0($t0)
    lw $s3, -12($fp)

    # Załadowanie p0, p1, p2 do $t0..$t5
    la $a0, points
    lhu $t0, 0($a0)
    lhu $t1, 2($a0)
    lhu $t2, 4($a0)
    lhu $t3, 6($a0)
    lhu $t4, 8($a0)
    lhu $t5, 10($a0)
    sll $t0, $t0, 16
    sll $t1, $t1, 16
    sll $t2, $t2, 16
    sll $t3, $t3, 16
    sll $t4, $t4, 16
    sll $t5, $t5, 16

draw_loop:
    # Ustawienie bazowej maski bitowej
    li $s0, 0x80
    # Wyznaczenie współczynników
    # $t6 = (1-t)
    subu $t6, $s7, $s6
    # $t7 = (1-t)^2
    multu $t6, $t6
    mflo $t7
    mfhi $a3
    srl $t7, $t7, 16
    sll $a3, $a3, 16
    or $t7, $t7, $a3
    # $t8 = 2*(1-t)*t
    sll $t8, $t6, 1
    multu $t8, $s6
    mflo $t8
    mfhi $a3
    srl $t8, $t8, 16
    sll $a3, $a3, 16
    or $t8, $t8, $a3
    # $t9 = t^2
    multu $s6, $s6
    mflo $t9
    mfhi $a3
    srl $t9, $t9, 16
    sll $a3, $a3, 16
    or $t9, $t9, $a3

    # Wyznaczenie współrzędnej x
    # $a0 = (1-t)^2 * x0
    multu $t7, $t0
    mflo $a0
    mfhi $a3
    srl $a0, $a0, 16
    sll $a3, $a3, 16
    or $a0, $a0, $a3
    # $a1 = 2*(1-t)*t * x1
    multu $t8, $t2
    mflo $a1
    mfhi $a3
    srl $a1, $a1, 16
    sll $a3, $a3, 16
    or $a1, $a1, $a3
    # $a0 = (1-t)^2 * x0 + 2*(1-t)*t * x1
    addu $a0, $a0, $a1
    # $a1 = t^2 * x2
    multu $t9, $t4
    mflo $a1
    mfhi $a3
    srl $a1, $a1, 16
    sll $a3, $a3, 16
    or $a1, $a1, $a3
    # $a0 = (1-t)^2 * x0 + 2*(1-t)*t * x1 + t^2 * x2
    addu $a0, $a0, $a1
    # $a0 zawiera współrzędną x

    # Wyznaczenie współrzędnej y
    # $a1 = (1-t)^2 * y0
    multu $t7, $t1
    mflo $a1
    mfhi $a3
    srl $a1, $a1, 16
    sll $a3, $a3, 16
    or $a1, $a1, $a3
    # $a2 = 2*(1-t)*t * y1
    multu $t8, $t3
    mflo $a2
    mfhi $a3
    srl $a2, $a2, 16
    sll $a3, $a3, 16
    or $a2, $a2, $a3
    # $a1 = (1-t)^2 * y0 + 2*(1-t)*t * y1
    addu $a1, $a1, $a2
    # $a2 = t^2 * y2
    multu $t9, $t5
    mflo $a2
    mfhi $a3
    srl $a2, $a2, 16
    sll $a3, $a3, 16
    or $a2, $a2, $a3
    # $a1 = (1-t)^2 * y0 + 2*(1-t)*t * y1 + t^2 * y2
    addu $a1, $a1, $a2
    # $a1 zawiera współrzędną y

    # Obcięcie części ułamkowej (x, y)
    sra $a0, $a0, 16
    sra $a1, $a1, 16

    # Modulo przez rozmiar obrazka
    divu $a0, $s1
    mfhi $a0
    divu $a1, $s2
    mfhi $a1

    # (szerokość wiersza * y) + (x / 8)
    multu $s3, $a1
    mflo $v0
    # $a3 = x % 8
    and $a3, $a0, 0x07
    sra $v1, $a0, 3
    addu $v0, $v0, $v1
    addu $v0, $v0, $s4
    lbu $a2, 0($v0)
    srlv $a3, $s0, $a3
    or $a2, $a2, $a3
    sb $a2, 0($v0)

    # Warunek końcowy
    blt $s6, $s7, draw_loop
    addu $s6, $s6, $s5

write_buffer:
    # Zapisanie bufora
    lw $a0, 0($fp)
    lw $a1, -8($fp)
    lw $a2, -4($fp)
    li $v0, 15
    syscall

    bltz $v0, fwrite_error
    nop

fclose:
    lw $a0, 0($fp)
    li $v0, 16
    syscall

exit:
    # Normalne zakończenie działania
    li $v0, 10
    syscall

fopen_error:
fwrite_error:
calc_size_error:
malloc_error:
    li $v0, 4
    la $a0, error_msg
    syscall
    li $v0, 17
    li $a0, 1
    syscall

    .data

error_msg:
    .asciiz "Error\n"
